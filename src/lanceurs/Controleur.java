
package lanceurs;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.LinkedList;
import java.util.List;
import programmes.Exemple01;
import programmes.Exemple02;
import programmes.Question01;
import programmes.Question02;
import programmes.Question03;
import programmes.Question04;
import programmes.Question05;

/**
 *
 * @author rsmon
 */

public class Controleur {
   
        private List<String> listeProgrammes = new LinkedList<String>();
        private String       programmeSel    = null;
    
        public void init(){
        
            listeProgrammes.add("Exemple01");
            listeProgrammes.add("Exemple02");
            listeProgrammes.add("Question01");
            listeProgrammes.add("Question02");
            listeProgrammes.add("Question03");
            listeProgrammes.add("Question04");
            listeProgrammes.add("Question05");  
             
        }
        
        public void executer(){
        
           if       (programmeSel.equals("Exemple01"))  new Exemple01().executer();
           else if  (programmeSel.equals("Exemple02"))  new Exemple02().executer();
           else if  (programmeSel.equals("Question01")) new Question01().executer();
           else if  (programmeSel.equals("Question02")) new Question02().executer();
           else if  (programmeSel.equals("Question03")) new Question03().executer();
           else if  (programmeSel.equals("Question04")) new Question04().executer();
           else if  (programmeSel.equals("Question05")) new Question05().executer();
        
        }
        
    
        //<editor-fold defaultstate="collapsed" desc="comment">
        
        public static final String PROP_LISTEPROGRAMMES = "listeProgrammes";
        
        public List<String> getListeProgrammes() {
            return listeProgrammes;
        }
        
        public void setListeProgrammes(List<String> listeProgrammes) {
            List<String> oldListeProgrammes = this.listeProgrammes;
            this.listeProgrammes = listeProgrammes;
            propertyChangeSupport.firePropertyChange(PROP_LISTEPROGRAMMES, oldListeProgrammes, listeProgrammes);
        }

               
    public static final String PROP_PROGRAMMESEL = "programmeSel";

    public String getProgrammeSel() {
        return programmeSel;
    }

    public void setProgrammeSel(String programmeSel) {
        String oldProgrammeSel = this.programmeSel;
        this.programmeSel = programmeSel;
        propertyChangeSupport.firePropertyChange(PROP_PROGRAMMESEL, oldProgrammeSel, programmeSel);
    }
        
        private transient final PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
        
        public void addPropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.addPropertyChangeListener(listener);
        }
        
        public void removePropertyChangeListener(PropertyChangeListener listener) {
            propertyChangeSupport.removePropertyChangeListener(listener);
        }
        
        
        void effacerConsole() {
         
          try {
          
            Robot pressbot = new Robot();
       
            pressbot.mouseMove(400, 400);
            pressbot.mousePress(InputEvent.BUTTON3_MASK);
            pressbot.keyPress(KeyEvent.VK_CONTROL);
            pressbot.keyPress(KeyEvent.VK_L); 
            pressbot.keyRelease(KeyEvent.VK_L); 
            pressbot.keyRelease(KeyEvent.VK_CONTROL); 
            pressbot.keyPress(KeyEvent.VK_CLEAR);
         }  
         catch (AWTException ex) {}
         
     
         
                   
    }       
       //</editor-fold>


    
  
}

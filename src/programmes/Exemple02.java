package programmes;

import static leclubdejudo.ClubJudo.listeDesJudokas;
import static leclubdejudo.ClubJudo.ageJudoka;
import static leclubdejudo.ClubJudo.categorieJudoka;
import leclubdejudo.Judoka;
import static utilitaires.UtilDate.aujourdhuiChaine;
import static utilitaires.UtilDate.convDateVersChaine;
import static leclubdejudo.Tris.trierParNomPrenom;

public class Exemple02 {

  public void executer() {
        
    afficherTitre();   
    trierLaListeDesJudokas();
    
    for( Judoka judoka : listeDesJudokas ) { traiterJudoka( judoka ); } 
    
    System.out.println();  
  }
  
   void afficherTitre() { System.out.printf("\n Liste des judokas du Club agés de 22 ans au: %10s\n\n", aujourdhuiChaine()); }
 
   void trierLaListeDesJudokas() { trierParNomPrenom(listeDesJudokas);}
   
   void traiterJudoka(Judoka pJudoka) {
      
        if( ageJudoka(pJudoka) == 22) { 
        
           String categMembre       = categorieJudoka(pJudoka);      
           String dateNaiss         = convDateVersChaine(pJudoka.dateNaiss); 
   
           System.out.printf(" %-10s %-10s %-10s %-20s\n", pJudoka.prenom, pJudoka.nom, dateNaiss, categMembre );
        }
    }  
}


